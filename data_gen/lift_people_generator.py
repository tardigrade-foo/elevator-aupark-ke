#!/usr/bin/python3

import sys
import configparser
import numpy
import operator
import random

config = configparser.ConfigParser()
config.read_file( sys.stdin )

def parse_time( line ):
    value = line.split( ':' )
    return 3600 * int( value[0] ) + 60 * int( value[1] )

def parse_floors( line ):
    lvl = []
    wg  = []
    for item in line.split( ' ' ):
        value = item.split( ':' )
        lvl.append( int( value[0] ) )
        wg.append( float( value[1] ) )
    
    if ( sum( wg ) != 1.0 ):
        print( 'Sum of from weights is not 1', file = sys.stderr )
        return

    return lvl, wg

def calc_section( section, name ):
    if ( not 'mean' in section ):
        print( 'Missing field mean in {}'.format( name ), file = sys.stderr )
        return
    if ( not 'stdev' in section ):
        print( 'Missing field stdev in {}'.format( name ), file = sys.stderr )
        return
    if ( not 'count' in section ):
        print( 'Missing field count in {}'.format( name ), file = sys.stderr )
        return
    if ( not 'from' in section ):
        print( 'Missing field from in {}'.format( name ), file = sys.stderr )
        return
    if ( not 'to' in section ):
        print( 'Missing field to in {}'.format( name ), file = sys.stderr )
        return

    mean  = parse_time( section['mean'] )
    stdev = parse_time( section['stdev'] )
    count = int( section['count'] )

    group_min    = section.getint( 'group_min', fallback = 0 )
    group_max    = section.getint( 'group_max', fallback = 0 )
    group_create = section.getfloat( 'group_create', fallback = 0 )

    begin = parse_time( section.get( 'begin', fallback = '0:00' ) )
    end   = parse_time( section.get( 'end',   fallback = '24:00' ) )

    from_lvl, from_wg = parse_floors( section['from'] )
    to_lvl,   to_wg   = parse_floors( section['to'] )

    events = []
    while ( count > 0 ):
        while ( True ):
            ts = int( round( numpy.random.normal( mean, stdev ) ) )
            if ( ( ts >= begin ) and ( ts <= end ) ):
                break;
        
        from_lvl_rnd = numpy.random.choice( from_lvl, p = from_wg )
        to_lvl_rnd   = numpy.random.choice( to_lvl, p = to_wg )
        
        if ( ( count > group_min ) and ( random.random() < group_create ) ):
            group_count      = random.randint( group_min, group_max )
            group_push_count = random.randint( group_min, group_count )
        else:
            group_count      = 1
            group_push_count = 1
        
        for i in range( group_push_count ):
            events.append( ( ts, from_lvl_rnd, to_lvl_rnd, 1 ) )
        for i in range( group_count - group_push_count ):
            events.append( ( ts, from_lvl_rnd, to_lvl_rnd, 0 ) )
        
        count -= group_count

    return events

occ_mean  = config.getfloat( 'DEFAULT', 'occupancy_mean', fallback = 1 )
occ_stdev = config.getfloat( 'DEFAULT', 'occupancy_stdev', fallback = 0 )

push_mean  = config.getint( 'DEFAULT', 'push_again_mean', fallback = 0 )
push_stdev = config.getint( 'DEFAULT', 'push_again_stdev', fallback = 0 )
push_min   = config.getint( 'DEFAULT', 'push_again_min', fallback = 0 )
push_never = config.getfloat( 'DEFAULT', 'push_again_never', fallback = 1 )

print( '#timestamp from to occupancy_limit push_again_time pushing_button' )
events = []
for section in config.sections():
    events.extend( calc_section( config[section], section ) )

events.sort( key = operator.itemgetter( 0 ) )

for event in events:
    occ_limit  = numpy.random.normal( occ_mean, occ_stdev )
    if ( occ_limit > 1 ):
        occ_limit = 1

    push_again = random.random()
    if ( push_again > push_never ):
        push_time = int( round( numpy.random.normal( push_mean, push_stdev ) ) )
        if ( push_time < push_min ):
            push_time = push_min
    else:
        push_time = 0

    print( '{} {} {} {:4.2f} {} {}'.format( event[0], event[1], event[2], occ_limit, push_time, event[3] ) )

