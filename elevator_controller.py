import socket
import sys
import time
import argparse


# TODO handle exceptions everywhere


class ElevatorController:
    def __init__(self):
        pass

    def shutdown(self):
        pass

    def send_msg(self, msg):
        pass

    def recv_msg_blocking(self):
        pass

    def request_beep(self, time_step, current_floor, target_floor ):
        """
        time_step:     current simulation time
        current_floor: floor the user called the elevator from
        target_floor:  floor the user wants to get to

        returns: (time_step, assigned_elevator)
        returns None if we get an unexpected reply
        """
        # in:  type==beep, time step, current floor, target floor, 
        # out: type==beep, time step, elevator number
        self.send_msg( "beep {} {} {}".format( time_step, current_floor, target_floor ) )
        reply = self.recv_msg_blocking()
        parts = reply.split()
        if parts[0] == "stop":
            print("Error in controller algorithm", file=sys.stderr)
            return None
        if parts[0] != "beep":
            print("Unexpected reply (incorrect message type {})!".format(parts[0]), file=sys.stderr)
            return None
        if len(parts) < 3:
            print("Unexpected reply (too few fields)!", file=sys.stderr)
            return None
        try:
            return (int(parts[1]), int(parts[2]))
        except ValueError:
            print("Unexpected reply (unexpected data type)!", file=sys.stderr)
            return None

    def request_update(self, time_step, elevator_states):
        """
        time_step:     current simulation time
        elevator_time: list of (floor, state) tuples

        returns: (time_step, [(floor, command), ..., (floor, command)])
        returns None if we get an unexpected reply
        """
        # in:  type==update, time step, [(elev_1_position,elev_1_state), ..., (elev_n_position, elev_n_state)]
        # out: type==update, time step, [(elev_1_position,elev_1_command), ..., (elev_n_position, elev_n_command)]
        states_str = " ".join([ "{} {}".format(pos, state) for (pos, state) in elevator_states])
        msg = "update {} {} {}".format(time_step, len(elevator_states), states_str)
        self.send_msg( msg )
        reply = self.recv_msg_blocking()
        parts = reply.split()
        if parts[0] == "stop":
            print("Error in controller algorithm", file=sys.stderr)
            return None
        if parts[0] != "update":
            print("Unexpected reply (incorrect message type)!", file=sys.stderr)
            return None
        if len(parts) < 3:
            print("Unexpected reply (too few fields)!", file=sys.stderr)
            return None
        try:
            time_step  = int(parts[1])
            elev_count = int(parts[2])
            elev_commands = []
            if len(parts) < 3 + 2 * elev_count:
                print("Unexpected reply (too few fields)!", file=sys.stderr)
                return None
            for i in range(elev_count):
                position = parts[ 3 + i * 2 ]
                command  = parts[ 3 + i * 2 + 1 ]
                elev_commands.append( ( int(position), command ) )
            return (time_step, elev_commands)
        except ValueError:
            print("Unexpected reply (unexpected data type)!", file=sys.stderr)
            return None

    def request_end(self):
        """
        Tell the elevator controller that we're done.

        returns nothing.
        """
        self.send_msg("stop")

class SocketElevatorController(ElevatorController):
    def __init__(self):
        super().__init__()
        while True:
            try:
                self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.sock.connect(("127.0.0.1", 4242))
                self.recvbuf = b""
                break
            except ConnectionRefusedError:
                print("elevator controller not yet running, waiing on 127.0.0.1 on port 4242...", file=sys.stderr)
                time.sleep( 1 )

    def send_msg(self, msg):
        # TODO check that the msg does not contain newline
        print( "Sending message: '{}'".format(msg) , file=sys.stderr)
        self.sock.sendall( msg.encode("utf8") + b"\n" )

    def recv_msg_blocking(self):
        # TODO timeout
        while True:
            self.recvbuf += self.sock.recv(128)
            if b"\n" in self.recvbuf:
                (msg, newline, self.recvbuf) = self.recvbuf.partition(b"\n")
                msg = msg.decode("utf8")
                print("Received message: '{}'".format(msg), file=sys.stderr)
                return msg


class StdioElevatorController(ElevatorController):
    def __init__(self):
        super().__init__()

    def shutdown(self):
        pass

    def send_msg(self, msg):
        if "\n" in msg:
            print( "messages cannot contain newlines!" , file=sys.stderr)
            sys.exit(-1)
        print( "Simulator sending message: '{}'".format(msg) , file=sys.stderr)
        print(msg)
        sys.stdout.flush()

    def recv_msg_blocking(self):
        msg = input()
        print("Simulator received message: '{}'".format(msg), file=sys.stderr)
        return msg

def parse_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter )
    parser.add_argument("communication",
                        help="How the simulator should be communicated with: 'socket' or 'stdio' ",
                        choices=["socket", "stdio"])
    return parser.parse_args()

if __name__ == "__main__":
    args = parse_args()
    api = None
    if args.communication == "socket":
        api = SocketElevatorController()
    elif args.communication == "stdio":
        api = StdioElevatorController()

    # Example:
    # going from 4 to 8 at time 1
    (time, assigned_elevator) = api.request_beep( 1, 4, 8 )
    # state of 4 elevators at time 1
    (time, elevator_commands) = api.request_update( 1, [(1, "is_closed"), (1, "moving_up"), (5, "moving_down"), (7, "is_open")] )

    api.request_end()
    api.shutdown()
