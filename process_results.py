#!/usr/bin/python3

import argparse
import math


def percentile_on_sorted(data, percentile):
    """Get value at specified percentile from a sorted array"""
    size = len(data)
    return data[int(math.ceil((size * percentile) / 100)) - 1]

def graph( name, time_values, time_granularity, time_range, max_text_columns, histogram_only = False ):
    print("-" * 100)
    time_values = sorted(time_values)
    print( "#", name, "#" )
    print()
    print( "Graph legend: '=': full column, ':': half column, '>>': out of scale" )
    print()

    def graph_str( value, units_per_column ):
        columns = int(value / units_per_column)
        graph_str = "=" * min( columns, max_text_columns )
        if columns >= max_text_columns:
            graph_str += ">>"
        elif (value % units_per_column) >= (units_per_column / 2):
            graph_str += ":" # half
        return graph_str

    if not histogram_only:
        print( "## Basic stats ##" )
        print( "* registered arrivals: {}".format( len(time_values ) ) )
        print( "* fastest time:        {}s".format( min(time_values ) ) )
        print( "* slowest time:        {}s".format( max(time_values ) ) )
        print( "* average time:        {:0.1f}s".format( sum(time_values ) / len(time_values) ) )
        print()
        print( "## Percentiles ##" )

        max_expected_time = 800
        time_per_column = max_expected_time / max_text_columns # guarantees we always fit on a X-line terminal
        print( "{:>5} {:>5} {:>7} {}".format( "%", "time", "persons", "time graph" ) )
        for percentile in [50, 75, 90, 95, 99, 99.9, 100]:
            time = percentile_on_sorted( time_values, percentile )
            people_in_percentile = sum( 1 for value in time_values if value <= time )
            preamble =  "{:>5.1f} {:>4}s {:>7}".format(percentile, time, people_in_percentile )
            print( preamble, graph_str( time, time_per_column ) )
        print()
    print( "## Time histogram ##" )
    min_time = min(time_values)
    max_time = max(time_values)
    people_per_column = len(total_times) / max_text_columns # guarantees we always fit on a X-line terminal

    print( "{:>5} {:>7} {:>7}".format( "time", "persons", "persons graph" ) )
    def hist_line( bucket_min, bucket_max ):
        if bucket_max is None:
            range_str = ">" + str(bucket_min) + "s"
            def pred(value): return value >= bucket_min
        else:
            range_str = "<" + str(bucket_max) + "s"
            def pred(value): return (value >= bucket_min and value < bucket_max)
        people_in_bucket = sum( 1 for value in time_values if pred(value) )
        preamble         = "{:<5} {:<7}".format( range_str, people_in_bucket )
        print( preamble, graph_str( people_in_bucket, people_per_column ) )

    for bucket_min in range(0, time_range, time_granularity):
        hist_line( bucket_min, bucket_min + time_granularity )
    hist_line( time_range, None )



if __name__=="__main__":
    parser = argparse.ArgumentParser( description='' )
    parser.add_argument( 'data', type=str, help='filepath to output data written by simulator', default='output.txt' )
    parser.add_argument( '--graph-scale', type=int, help='scale of graphs to draw', default=80 )
    parser.add_argument( '--time-granularity', type=int, help='size of buckets in time histogram', default=30 )
    parser.add_argument( '--time-range',       type=int, help='total range of times covered by time histogram', default=450 )
    args = parser.parse_args()

    total_times     = []
    times_transport = []
    times_waiting   = []
    times_arrival   = []
    people_by_elevator = [0, 0, 0, 0]
    with open( args.data ) as f:
        for line in f:
            parts = line.split()
            total_times.append( int(parts[0]) )
            times_transport.append( int(parts[1]) )
            times_waiting.append( int(parts[2]) )
            times_arrival.append( int(parts[3]) )
            people_by_elevator[int(parts[4])] += 1
    graph( "time from arrival to target floor", total_times, args.time_granularity, args.time_range, args.graph_scale )
    graph( "time spent in elevator", times_transport, args.time_granularity, args.time_range, args.graph_scale )
    graph( "time spent waiting for elevator", times_waiting, args.time_granularity, args.time_range, args.graph_scale )
    graph( "arrival time", times_arrival, 3600, 86400, args.graph_scale, True )
    print()
    print( "# people transported per elevator #" )
    print( ",".join([str(e) for e in people_by_elevator]) )


