#!/usr/bin/python3

import socket
import sys
import configparser
import argparse

recvbuf = b""

def recv_msg_blocking(recvbuf, conn):
  while True:
    recvbuf += conn.recv(128)
    if b"\n" in recvbuf:
      (msg, newline, recvbuf) = recvbuf.partition(b"\n")
      msg = msg.decode("utf8")
      print("Controller received message: '{}'".format(msg))
      return msg

def send_msg(conn, msg):
  if "\n" in msg:
    print( "messages cannot contain newlines!" )
    sys.exit(-1)
  print( "Controller sending message: '{}'".format( msg ) )
  conn.sendall(msg.encode("utf8") + b"\n")

if __name__=='__main__':
  parser = argparse.ArgumentParser( description='' )
  parser.add_argument( '--config', type=str, help='configuration file' )
  args = parser.parse_args()
  
  configpath = args.config if args.config else '../config.ini'
    
  config = configparser.ConfigParser()  
  if not config.read( configpath ):
    print( "config '{}' does not exist".format( configpath ), file=sys.stderr )
    sys.exit( -1 )    
    
  try:    
    settings      = config[ 'default' ]
    elevators_count = int( settings[ 'num_of_elevators' ] )
    max_floor     = int( settings[ 'floor_max' ] )
    min_floor     = int( settings[ 'floor_min' ] )
    people_counter = 0
  except:
    print( "setting simulator failed", file=sys.stderr )
    sys.exit( -1 )

  with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind( ( "127.0.0.1", 4242 ) )
    s.listen()
    conn, addr = s.accept()
        
    with conn:
      print( 'Connected by', addr )
                
      next_actions = [ { 'moving_up' : True } for x in range( elevators_count ) ]
          
      while True:
        msg = recv_msg_blocking(recvbuf, conn)
        
        if msg.startswith("stop"):
          print("stopping...")
          break
        
        # Person beep
        elif msg.startswith("beep"):
          beep_param = msg.split()
          selected_elevator = people_counter % elevators_count
          people_counter += 1
          send_msg( conn, "beep {} {}".format( beep_param[1], selected_elevator ) )
        
        # State update of all elevators
        elif msg.startswith("update"):
          received = msg.split()
          result = "update {} {}".format( received[ 1 ], received[ 2 ] )
          
          for i in range( elevators_count ):
            next_action = next_actions[ i ]
            
            floor = int( received[ 3 + i*2 ] )
            state = str( received[ 3 + i*2 + 1 ] )

            if state == 'is_open':
              set_action = 'close'
              set_floor  = floor

            elif state == 'is_closing':
              set_action = 'close'
              set_floor  = floor

            elif state == 'is_closed':
              set_action = 'up' if next_actions[ i ]['moving_up'] else 'down'
              set_floor  = floor

              if next_action['moving_up'] and floor == max_floor-1:
                next_actions[ i ]['moving_up'] = False
              elif not next_action['moving_up'] and floor == min_floor+1:
                next_actions[ i ]['moving_up'] = True

            elif state == 'moving_up':
              set_action = 'open'
              set_floor  = floor

            elif state == 'moving_down':
              set_action = 'open'
              set_floor  = floor

            result += " {} {}".format( set_floor, set_action )                            
            
          send_msg( conn, result )
        
        else:
          print( "Unexpected message", msg )
          break


 
