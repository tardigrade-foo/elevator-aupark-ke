#!/usr/bin/python3

import socket

recvbuf = b""

def recv_msg_blocking(recvbuf, conn):
    while True:
        recvbuf += conn.recv(128)
        if b"\n" in recvbuf:
            (msg, newline, recvbuf) = recvbuf.partition(b"\n")
            msg = msg.decode("utf8")
            print("Controller received message: '{}'".format(msg))
            return msg

def send_msg(conn, msg):
    if "\n" in msg:
        print( "messages cannot contain newlines!" )
        sys.exit(-1)
    print( "Controller sending message: '{}'".format( msg ) )
    conn.sendall(msg.encode("utf8") + b"\n")

port = 4242
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(("127.0.0.1", port))
    s.listen()
    conn, addr = s.accept()
    with conn:
        print( 'Connected by', addr )

        msg = recv_msg_blocking(recvbuf, conn)
        #expected beep 1 0 4
        send_msg( conn, 'beep 1 0' )

        msg = recv_msg_blocking(recvbuf, conn)
        #expected update 3 4 0 is_closed 0 is_closed 0 is_closed 0 is_closed
        send_msg( conn, 'update 3 4 0 open 0 close 0 close 0 close')
        
        msg = recv_msg_blocking(recvbuf, conn)
        #expected update 6 4 0 is_open 0 is_closed 0 is_closed 0 is_closed
        send_msg( conn, 'update 6 4 0 close 0 close 0 close 0 close')

        msg = recv_msg_blocking(recvbuf, conn)
        #expected update 9 4 0 is_closing 0 is_closed 0 is_closed 0 is_closed
        send_msg( conn, 'update 9 4 0 close 0 close 0 close 0 close')

        msg = recv_msg_blocking(recvbuf, conn)
        #expected update 12 4 0 is_closed 0 is_closed 0 is_closed 0 is_closed
        send_msg( conn, 'update 12 4 0 up 0 close 0 close 0 close')

        msg = recv_msg_blocking(recvbuf, conn)
        #expected update 15 4 0 moving_up 0 is_closed 0 is_closed 0 is_closed
        send_msg( conn, 'update 15 4 0 up 0 close 0 close 0 close')

        msg = recv_msg_blocking(recvbuf, conn)
        #expected update 18 4 1 moving_up 0 is_closed 0 is_closed 0 is_closed
        send_msg( conn, 'update 18 4 1 up 0 close 0 close 0 close')

        msg = recv_msg_blocking(recvbuf, conn)
        #expected update 21 4 2 moving_up 0 is_closed 0 is_closed 0 is_closed
        send_msg( conn, 'update 21 4 2 up 0 close 0 close 0 close')

        msg = recv_msg_blocking(recvbuf, conn)
        #expected update 24 4 3 moving_up 0 is_closed 0 is_closed 0 is_closed
        send_msg( conn, 'update 24 4 3 open 0 close 0 close 0 close')

        msg = recv_msg_blocking(recvbuf, conn)
        #expected update 27 4 4 is_open 0 is_closed 0 is_closed 0 is_closed
        send_msg( conn, 'update 27 4 4 close 0 close 0 close 0 close')

        msg = recv_msg_blocking(recvbuf, conn)
        #expected stop, job done 


