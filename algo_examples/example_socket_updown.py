#!/usr/bin/python3

import socket
import sys
import configparser
import argparse

recvbuf = b""
elevator = {'floor':0,'state':'open', 'way':'up'}
elevators = []
people_counter = 0

def recv_msg_blocking(recvbuf, conn):
    while True:
        recvbuf += conn.recv(128)
        if b"\n" in recvbuf:
            (msg, newline, recvbuf) = recvbuf.partition(b"\n")
            msg = msg.decode("utf8")
            print("Controller received message: '{}'".format(msg))
            return msg

def send_msg(conn, msg):
    if "\n" in msg:
        print( "messages cannot contain newlines!" )
        sys.exit(-1)
    print( "Controller sending message: '{}'".format( msg ) )
    conn.sendall(msg.encode("utf8") + b"\n")

port = 4242

parser = argparse.ArgumentParser( description='' )
parser.add_argument( '--config', type=str, help='configuration file' )
args = parser.parse_args()

configpath = args.config if args.config else '../config.ini'
  
config = configparser.ConfigParser()  
if not config.read( configpath ):
  print( "config '{}' does not exist".format( configpath ), file=sys.stderr )
  sys.exit( -1 )    
  
try:    
  settings      = config[ 'default' ]
  elevators_count = int( settings[ 'num_of_elevators' ] )
  max_floor     = int( settings[ 'floor_max' ] )
  min_floor     = int( settings[ 'floor_min' ] )
except:
  print( "setting simulator failed", file=sys.stderr )
  sys.exit( -1 )

for i in range(0, elevators_count):
    elevators.append(elevator.copy())

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(("127.0.0.1", port))
    s.listen()
    conn, addr = s.accept()
    with conn:
        print( 'Connected by', addr )
        while True:
            msg = recv_msg_blocking(recvbuf, conn)
            if msg.startswith("stop"):
                print("stopping...")
                break
            # Person beep
            elif msg.startswith("beep"):
                beep_param = msg.split()
                selected_elevator = people_counter % elevators_count
                people_counter += 1
                # reply: time step, elevator 5 assigned to the person who beeped
                send_msg( conn, "beep {} {}".format(beep_param[1],selected_elevator))
            # State update of all elevators
            elif msg.startswith("update"):
                result_msg = "update "
                update_param = msg.split()
                result_msg = result_msg + update_param[1] + " "
                if int(update_param[2]) != elevators_count:
                    print('Elevators count does not match')
                    sys.exit(-1)
                result_msg = result_msg + str(elevators_count) + " "
                update_index = 3
                elevator_index = 1
                for elevator in elevators:
                    floor = update_param[update_index]
                    update_index += 1
                    state = update_param[update_index]
                    update_index += 1
                    #print( 'Elevator {} state {} way {}'.format(elevator_index, elevator['state'], elevator['way']))
                    
                    # CLOSE
                    if elevator['state'] == 'close':
                        if str(elevator['floor']) != floor:
                            print('Elevator {} is on floor {}, but expected {}'.format(elevator_index, floor, elevator['floor']))
                            send_msg( conn, 'stop' )
                            sys.exit(-1)
                        if state.startswith('moving'):
                            print('Elevator {} unexpectedly moving'.format(elevator_index))
                            send_msg( conn, 'stop' )
                            sys.exit(-1)
                            
                        elif state == 'is_open' or state == 'is_closing':
                            result_msg = result_msg + str( elevator['floor'] ) + ' close '
                        elif state == 'is_closed':
                            elevator['state'] = 'move'
                            result_msg = result_msg + str(elevator['floor'])
                            if elevator['way'] == 'up' and elevator['floor'] == max_floor:
                                elevator['way'] = 'down'
                            elif elevator['way'] == 'down' and elevator['floor'] == min_floor:
                                elevator['way'] = 'up'
                            result_msg = result_msg + ' ' + elevator['way'] + ' '
                        else:
                            print( 'Unrecognized update')
                            send_msg( conn, 'stop' )
                            sys.exit(-1)
                        
                    # OPEN
                    elif elevator['state'] == 'open':
                        if str(elevator['floor']) != floor:
                            print('Elevator {} is on floor {}, but expected {}'.format(elevator_index, floor, elevator['floor']))
                            send_msg( conn, 'stop' )
                            sys.exit(-1)
                            
                        if state == 'is_open':
                            elevator['state'] = 'close'
                            result_msg = result_msg + str( elevator['floor'] ) + ' close '
                        elif state == 'is_closed':
                            result_msg = result_msg + str( elevator['floor'] ) + ' open '
                        else:
                            print( 'Elevator moving unexpectedly' )
                            send_msg( conn, 'stop' )
                            sys.exit(-1)
                            
                    # MOVE
                    elif elevator['state'] == 'move':
                        if state == 'moving_up' and elevator['way'] == 'down':
                            print( 'Elevator moving to oposite way unexpectedly' )
                            send_msg( conn, 'stop' )
                            sys.exit(-1)
                        if state == 'moving_down' and elevator['way'] == 'up':
                            print( 'Elevator moving to oposite way unexpectedly' )
                            send_msg( conn, 'stop' )
                            sys.exit(-1)
                        if state == 'is_open' or state == 'is_closed':
                            print( 'Elevator is open unexpectedly' )
                            send_msg( conn, 'stop' )
                            sys.exit(-1)
                        
                        print( 'Elevator {} expected {} and obtained {}'.format( elevator_index, int(floor), elevator['floor'] ) )
                        if elevator['way'] == 'up':
                            if int(floor) == elevator['floor']:
                                result_msg = result_msg + floor + ' open '
                                elevator['floor'] = int(floor) + 1
                                elevator['state'] = 'open'
                            else:
                                print( 'Elevator on unexpected floor' )
                                send_msg( conn, 'stop' )
                                sys.exit(-1)
                        else:
                            if int(floor) == elevator['floor']:
                                result_msg = result_msg + floor + ' open '
                                elevator['floor'] = int(floor) - 1
                                elevator['state'] = 'open'
                            else:
                                print( 'Elevator on unexpected floor' )
                                send_msg( conn, 'stop' )
                                sys.exit(-1)
                    else:
                        print( 'Unknown internal state' )
                        sys.exit(-1)
                    elevator_index += 1
                    
                send_msg( conn, result_msg )
            else:
                print( "Unexpected message", msg )
                break


	 
