import subprocess
import sys

def recv_msg_blocking(process):
    while True:
        msg = process.stdout.readline().decode("utf8")[:-1]
        print("Controller received message: '{}'".format(msg))
        return msg

def send_msg(process, msg):
    if "\n" in msg:
        print( "messages cannot contain newlines!" )
        sys.exit(-1)
    print( "Controller sending message: '{}'".format( msg ) )
    process.stdin.write( msg.encode("utf8") + b"\n")

process = subprocess.Popen(
    ["python3", "./elevator-controller.py", "stdio"],
    stdin=subprocess.PIPE,
    stdout=subprocess.PIPE,
    bufsize=0 )

while True:
    msg = recv_msg_blocking( process )
    if msg.startswith("stop"):
        print("stopping...")
        break
    # Person beep
    if msg.startswith("beep"):
        # reply: time step, elevator 5 assigned to the person who beeped
        send_msg( process, "beep 1 5" )
    # State update of all elevators
    elif msg.startswith("update"):
        # reply: 
        # time step,
        # we have 4 elevators,
        #   - elevator 1 at floor 1 should go up 
        #   - elevator 2 at floor 1 should open
        #   - elevator 3 at floor 5 should open
        #   - elevator 4 at floor 7 should close
        send_msg( process, "update 1 4 1 up 1 open 5 open 7 close" )
    else:
        print( "Unexpected message", msg )
        break





