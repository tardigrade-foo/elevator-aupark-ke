import socket

recvbuf = b""

def recv_msg_blocking(recvbuf, conn):
    while True:
        recvbuf += conn.recv(128)
        if b"\n" in recvbuf:
            (msg, newline, recvbuf) = recvbuf.partition(b"\n")
            msg = msg.decode("utf8")
            print("Controller received message: '{}'".format(msg))
            return msg

def send_msg(conn, msg):
    if "\n" in msg:
        print( "messages cannot contain newlines!" )
        sys.exit(-1)
    print( "Controller sending message: '{}'".format( msg ) )
    conn.sendall(msg.encode("utf8") + b"\n")

port = 4242
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(("127.0.0.1", port))
    s.listen()
    conn, addr = s.accept()
    with conn:
        print( 'Connected by', addr )
        while True:
            msg = recv_msg_blocking(recvbuf, conn)
            if msg.startswith("stop"):
                print("stopping...")
                break
            # Person beep
            elif msg.startswith("beep"):
                # reply: time step, elevator 5 assigned to the person who beeped
                send_msg( conn, "beep 1 5" )
            # State update of all elevators
            elif msg.startswith("update"):
                # reply: 
                # time step,
                # we have 4 elevators,
                #   - elevator 1 at floor 1 should go up 
                #   - elevator 2 at floor 1 should open
                #   - elevator 3 at floor 5 should open
                #   - elevator 4 at floor 7 should close
                send_msg( conn, "update 1 4 1 up 1 open 5 open 7 close" )
            else:
                print( "Unexpected message", msg )
                break
