#!/usr/bin/python3

import sys
import argparse
import configparser
import os.path
import math

from random import randrange
from collections import deque

from elevator_controller import SocketElevatorController as socket_controller
from elevator_controller import StdioElevatorController  as stdio_controller

class RuntimeStats:
  '''
  Data collected during runtime.

  Args:
    num_of_elevators: Total number of elevators.
  '''
  def __init__( self, num_of_elevators : int ):
    actions = { 
      'close' : 0, 
      'open'  : 0, 
      'up'    : 0, 
      'down'  : 0 
    }
    states  = { 
      'is_closed'   : 0, 
      'is_closing'  : 0, 
      'is_open'     : 0, 
      'moving_up'   : 0, 
      'moving_down' : 0 
    }

    self.num_elevators  = num_of_elevators
    self.people_arrived = 0
    self.people_served  = 0
    self.people_queue_skiped   = 0
    self.elevator_actions_send = [ actions.copy() for i in range( num_of_elevators ) ]
    self.elevator_states_count = [ states.copy()  for i in range( num_of_elevators ) ]

  def print( self, file ):
    print( "Printing runtime stats ...", file=file )
    print( "  People arrived: {}".format( config.stats.people_arrived ), file=file )
    print( "  People served:  {}".format( config.stats.people_served ), file=file )
    print( "  People used unassigned elevator: {}".format( config.stats.people_queue_skiped ), file=file )

    for i in range( config.num_elevators ):
      print( "  Elevator number {}".format( i ), file=file )

      sent_actions  = config.stats.elevator_actions_send[ i ]
      total_actions = sum( sent_actions.values() )  

      print( "  - ration of commands sent by controller algorithm".format( i ), file=file )
      print( "    'close': {} ({:3.2f}%) , 'open': {} ({:3.2f}%), 'up': {} ({:3.2f}%), 'down': {} ({:3.2f}%)".format( 
        sent_actions[ 'close' ], 100 * sent_actions[ 'close' ] / total_actions, 
        sent_actions[ 'open' ],  100 * sent_actions[ 'open' ]  / total_actions,
        sent_actions[ 'up' ],    100 * sent_actions[ 'up' ]    / total_actions,    
        sent_actions[ 'down' ],  100 * sent_actions[ 'down' ]  / total_actions ), file=file ) 

      last_states  = config.stats.elevator_states_count[ i ]
      total_states = sum( last_states.values() )

      print( "  - states where elevator spent time".format( i ), file=file )
      print( "    'is_closed': {} ({:3.2f}%), 'is_closing': {} ({:3.2f}%), 'is_open': {} ({:3.2f}%),"
             " 'moving_up': {} ({:3.2f}%), 'moving_down': {} ({:3.2f}%)".format(
        last_states[ 'is_closed' ],   100 * last_states[ 'is_closed' ]   / total_states, 
        last_states[ 'is_closing' ],  100 * last_states[ 'is_closing' ]  / total_states, 
        last_states[ 'is_open' ],     100 * last_states[ 'is_open' ]     / total_states,
        last_states[ 'moving_up' ],   100 * last_states[ 'moving_up' ]   / total_states, 
        last_states[ 'moving_down' ], 100 * last_states[ 'moving_down' ] / total_states ), file=file )


class Config:
  '''
  Simulator configuration

  Attributes:
    path_config:     Path to config file in .ini format.
    path_in:         Path to input data file.
    path_out:        Path to ouput file with statistics.
    controller:      Communication channel (stdio or socket).
    granularity:     Time granularity of elevator states.
    num_elevators:   Total number of elevators.
    floor_max:       The highest floor number.
    floor_min:       The lowest floor number.
    capacity_max:    Maximum elevator capacity.
    limit_after_eof: Time limit after EOF detected.
  '''
  def __init__( self, channel : str, path_config : str, path_in : str, path_out : str ):       
    # initialize config parser        
    config = configparser.ConfigParser()        
    if not config.read( configpath ):
      print( "Config .ini file '{}' does not exist".format( 
              configpath ), file=sys.stderr )
      self.shutdown()
      sys.exit( 1 ) 
    
    try:
      # parse .ini file and set config attributes
      settings         = config[ 'default' ] 
      self.path_config = path_config
      self.path_in     = path_in 
      self.path_out    = path_out
      self.controller  = stdio_controller() if channel == 'stdio' else socket_controller() 
      self.granularity = int( settings[ 'granularity' ] )
      self.num_elevators = int( settings[ 'num_of_elevators' ] )
      self.floor_max     = int( settings[ 'floor_max' ] )
      self.floor_min     = int( settings[ 'floor_min' ] )
      self.capacity_max  = list( map( int, settings[ 'elevator_capacity' ].split() ) )
      self.action_opts   = [ 'open', 'close', 'up', 'down' ]
      
      if len( self.capacity_max ) != self.num_elevators:
        print( "Config field 'elevator_capacity' of size {}, should be {}.".format(
          len( self.capacity_max ), self.num_elevators ), file=sys.stderr )
        self.shutdown()
        sys.exit( 1 )

      self.respect_queue   = ( settings[ 'respect_assigned_number' ] == 'yes' )
      self.beeping_again   = ( settings[ 'enable_beeping_again' ] == 'yes' )
      self.limit_after_eof = int( settings[ 'time_limit_after_eof' ] )
      self.stats = RuntimeStats( self.num_elevators ) 
    except:
      print( "Simulator initialization failed.", file=sys.stderr )
      self.shutdown()
      sys.exit( 1 )

  def shutdown( self ):
    if self.controller:
      self.controller.request_end();
      self.controller.shutdown()

class Person:
  '''
  Person representation
  
  Attributes: 
    time_arrival: Time of person arrival. 
    time_getin:   Time of entry into elevator.  
    source_floor: Floor where person arrived and called elevator.
    target_floor: Floor where person wish to go.    
  '''
  def __init__( self, time_arrival : int, source_floor : int, target_floor : int, beep_again : int ):
    self.time_arrival = time_arrival
    self.time_getin   = 0
    self.source_floor = source_floor
    self.target_floor = target_floor
    self.beep_again   = beep_again
    self.all_beeps = []

  def __str__( self ):
    '''
    Returns string representation of the person.
    '''
    return "{} {} {}".format( self.time_arrival, self.source_floor, self.target_floor )
  
  def __eq__( self, other ):  
    '''
    Implements rich comparison method 'equal to'.
    '''  
    return self.time_arrival == other.time_arrival and \
           self.source_floor == other.source_floor and \
           self.target_floor == other.target_floor  
     
  def on_target_reached( self, time : int ) -> int:
    '''
    Returns how long it took from source to targer floor.
    
    Args:
      time: Time when the target floor was reached.   
    Returns:
      Difference between time of arrival and time of target floor reaching.  
    '''
    return time - self.time_arrival

class Queue:
  '''
  Elevator waiting line representation.
  
  Attributes:
    container: List-like container with fast appends and pops. 
  '''
  def __init__( self, id ):
    self.id        = id
    self.container = deque()

  def __len__( self ):
    return len( self.container )

  def list( self ) -> list:
    return list( self.container )

  def at( self, index : int ) -> Person:
    return self.container[ index ]

  def push( self, person : Person ):
    '''
    Add person to elevator waiting line.
    
    Args:
      person: Person who gets into waiting line. 
    '''
    self.container.append( person )
  
  def pop( self ) -> Person:
    '''
    Remove person waiting the longest in the line.
    
    Returns:
      Person representation.
    '''
    if self.container:
      return self.container.pop()
    return None
    
  def empty( self ) -> bool:
    '''
    Check if the waiting line is empty or not.
    
    Returns:
      True if waiting line is empty, otherwise False.
    '''
    return not self.container

  def remove( self, person : Person ):
    if person:
      self.container.remove( person )

  def print( self ):
    print( [ str( person ) for person in self.container ], file=sys.stderr )
        
class State:
  '''
  Elevator abstract state.
  
  Attributes:
    name:      Name of the elevator state.
    floor:     Floor number.
    floor_min: The lowest floor number.
    floor_max: The highest floor number.
  '''
  def __init__( self, name : str, floor : int, floor_min : int, floor_max : int ):
    self.name     = name
    self.current  = floor
    self.min      = floor_min
    self.max      = floor_max
    self.duration = 0

  def __str__( self ):
    '''
    Returns string representation of state.
    '''
    return self.name
  
  def on_action( self, stats : RuntimeStats, action : str ):
    '''
    Make action and change elevator state based on it.
    
    Args:    
      action: Action to be taken (options are 'open', 'close', 'up' or 'down'). 
    '''
    pass

class StateDoorOpened( State ):
  '''
  Elevator state - door open. 
  '''
  def __init__( self, floor : int, floor_min : int, floor_max : int ):
    super().__init__( 'is_open', floor, floor_min, floor_max )    
  
  def on_action( self, stats : RuntimeStats, action : str ):
    '''
    With action 'close' the elevator door will start closing 'is_closing' 
    and after some time the elevator state will change to 'is_closed'.    
    '''
    self.duration = max( 0, self.duration - 1 )
  
    if self.duration == 0:
      return StateDoorClosing( self.current, self.min, self.max )     
    return self    

class StateDoorClosed( State ):
  '''
  Elevator state - door closed.
  '''
  def __init__( self, floor : int, floor_min : int, floor_max : int ):
    super().__init__( 'is_closed', floor, floor_min, floor_max )  
      
  def on_action( self, stats : RuntimeStats, action : str ):
    '''
    In the state 'is_closed', when the elevator doors are closed,
    the elevator can open them using command 'open', can move up
    using command 'up' or move down using command 'down'. In case 
    of 'close' command, elevator state will not change.
    '''
    if action == 'open':
      return StateDoorOpened( self.current, self.min, self.max )
    if action == 'up':
      if self.current < self.max:
        return StateMovingUp( self.current, self.min, self.max )
      else:
        print( "Elevator can't move up from the highest floor {}.".format( 
          self.current ), file=sys.stderr )
        return self
    if action == 'down':
      if self.current > self.min:
        return StateMovingDown( self.current, self.min, self.max )
      else:
        print( "Elevator can't move down from the lowest floor {}.".format(
          self.current ), file=sys.stderr )
        return self
    return self

class StateDoorClosing( State ):
  '''
  Elevator state - door closing.
  '''
  def __init__( self, floor : int, floor_min : int, floor_max : int):
    super().__init__( 'is_closing', floor, floor_min, floor_max )
  
  def on_action( self, stats : RuntimeStats, action: str ):
    '''
    The elevator doors are closing and elevator responses only to 'open'
    command by opening elevator doors.
    '''
    if action == 'open':
      return StateDoorOpened( self.current, self.min, self.max )
    return StateDoorClosed( self.current, self.min, self.max )

class StateMovingUp( State ):
  '''
  Elevator state - moving up.
  '''
  def __init__( self, floor : int, floor_min : int, floor_max : int ):
    super().__init__( 'moving_up', floor, floor_min, floor_max )  
      
  def on_action( self, stats : RuntimeStats, action: str ):
    '''
    The elevator is moving up and continue to move up only if 
    the highest floor is not reached. The elevator in this state 
    response to command 'open' by opening doors and to move commands,
    'up' and 'down', by moving to the desired direction.  
    '''
    if self.current >= self.max:
      print( "Elevator cannot move up from floor {} (the highest floor is {}).".format(
             self.current, self.max ), file=sys.stderr )
      sys.exit( 1 )
      
    self.current += 1      
    if action == 'open':
      return StateDoorOpened( self.current, self.min, self.max )
    elif action == 'close':
      return StateDoorClosed( self.current, self.min, self.max )
    elif action == 'down':
      return StateMovingDown( self.current, self.min, self.max )

    return self

class StateMovingDown( State ):
  '''
  Elevator state - moving down.
  '''
  def __init__( self, floor : int, floor_min : int, floor_max : int ):
    super().__init__( 'moving_down', floor, floor_min, floor_max )
        
  def on_action( self, stats : RuntimeStats, action : str ):
    '''
    The elevator is moving down and contnues to move down only if 
    the lowest floor is not reached. The elevator responds to the same
    action as the elevator in state 'moving_up'.
    '''
    if self.current <= self.min:
      print( "Elevator cannot move down from floor {} (the lowest floor is {})".format(
             self.current, self.min ), file=sys.stderr )
      sys.exit( 1 )
    self.current -= 1
    if action == 'open':
      return StateDoorOpened( self.current, self.min, self.max )
    elif action == 'close':
      return StateDoorClosed( self.current, self.min, self.max )
    elif action == 'up':
      return StateMovingUp( self.current, self.min, self.max )
    return self

class Elevator:
  '''
  Elevator representation.
   
  Attributes:
    number:       Elevator number.
    floor_min:    The lowest floor number.
    floor_max:    The highest floor number.
    state:        Current elevator state.
    capacity_max: Elevator maximum capacity.
    space:        Elevator space.
  '''
  def __init__( self, number : int, config : Config ):
    self.number    = number
    self.config    = config
    self.floor_min = config.floor_min
    self.floor_max = config.floor_max
    self.state = StateDoorClosed( 0, config.floor_min, config.floor_max )
    self.stops = dict.fromkeys( range( config.floor_min, config.floor_max + 1 ), 0 )
    self.space = []   

    try:
      self.capacity_max = config.capacity_max[ number ]
    except:
      print( "Invalid length of config field 'elevator_capacity'.", file=sys.stderr )
      self.config.shutdown()
      sys.exit( 1 )

  def print( self ):
    print( [ str( person ) for person in self.space ], file=sys.stderr )

  def has_stop_on_floor( self, floor_num : int ):
    '''
    '''
    return self.stops[ floor_num ] > 0

  def status( self ) -> list:
    '''
    Get current elevator status. 
    
    Returns:
      [ floor, name ]: Returns list containing current floor number and state name. 
    '''
    return [ self.state.current, self.state.name ] 
          
  def capacity( self ) -> int:
    '''
    Get current elevator capacity.    
    '''
    free_space = self.capacity_max - len( self.space )
    return 0 if free_space < 0 else free_space 
  
  def closing_steps( self, previous : int, count_people_in : int, count_people_out : int ) -> int:
    count_traffic = count_people_in + count_people_out

    if count_traffic == 0:
      return previous
    
    current = int( math.ceil( count_traffic / float( self.config.granularity ) ) )
    return current + previous

  def on_action( self, action : str ) -> State:
    '''
    Change current state based on given action.
    
    Args:
      action: Action to be taken ('open', 'close', 'up' or 'down').
    Returns:                                                 
      New elevator state changed based on action.
    '''
    self.config.stats.elevator_actions_send[ self.number ][ action ] += 1
    self.state = self.state.on_action( self.config.stats, action )  
    return self.state  
    
  def on_person_in( self, time : int, person : Person, to_assigned : bool ) -> bool:
    '''
    If there is free space in elevator, the person can get in.
    
    Args:
      time:   Current simulation time.
      person: Person trying to get into elevator.
    Returns:
      True if person successfully gets into elevator, False otherwise.
    '''
    if self.capacity() <= 0:
      return False

    if not to_assigned:
      self.config.stats.people_queue_skiped += 1

    person.time_getin = time    
    self.stops[ person.target_floor ] += 1
    self.space.append( person )
    return True
  
  def on_people_out( self, output, time : int, floor_number : int ) -> int:
    '''
    If the person's target floor is reached, the person can 
    get out the elevator.
    
    Args:
      output:       Output file descriptor.
      time:         Current simulation time.
      floor_number: Current floor number.
    '''
    to_be_removed = []
    people_served = 0

    for person in self.space:
      if person.target_floor == floor_number:
        
        delta_overall   = person.on_target_reached( time )
        delta_transport = time - person.time_getin
        delta_waiting   = person.time_getin - person.time_arrival
        
        for b in person.all_beeps:
          time_beep = b[ 0 ]
 
          output.write( "{} {} {} {} {} {} {}".format( delta_overall, delta_transport, delta_waiting, time_beep, self.number, person.source_floor, person.target_floor ) )
          
          for x in range( 1, len( b ), 2 ):
            floor_num = b[ x ] 
            state = b[ x+1 ]
                      
            output.write( " {} {}".format( floor_num, state ) )          
          output.write( "\n" )
        
        self.stops[ person.target_floor ] -= 1
        to_be_removed.append( person )
        people_served += 1

    for person in to_be_removed:
      self.space.remove( person )

    self.config.stats.people_served += people_served
    return people_served

class Floor:
  '''
  Floor representation.
  
  Attributes:
    number: Floor number. 
    queues: Elevator waiting lines - one queue for each elevator.
  '''
  def __init__( self, config : Config, floor_number : int ):
    self.config = config
    self.number = floor_number
    self.queues = [ Queue( i ) for i in range( config.num_elevators ) ]   

  def number_of_people_waiting( self ):
    return sum( [ len( q ) for q in self.queues ] )

  def on_person_arrival( self, person : Person, elevator_id : int ):
    '''
    Add person to waiting line based on elevator number he/she is waiting for.
    
    Args:
      person:      Person who waits for elevator.
      elevator_id: Elevator number assigned to the person.
    '''
    queue = self.queues[ elevator_id ]
    queue.push( person )
  
  def on_door_opening( self, output, time : int, elevator : Elevator ):
    '''
    When elevator door opens, people who reached their target floor will 
    get out of the elevator and others, waiting outside, will try to get in.
    
    Args:
      output:   Output file descriptor.
      time:     Current simulation time.
      elevator: Elevator representation. 
     
    '''
    index = elevator.number
    queue = self.queues[ index ]

    count_people_out = elevator.on_people_out( output, time, self.number )
    count_people_in  = 0 

    while not queue.empty() and elevator.capacity() > 0:    
      person = queue.pop()
      if not elevator.on_person_in( time, person, True ):
        print( "Person can't get into elevator.", file=sys.stderr )
        self.config.shutdown()
        sys.exit( 1 )
      count_people_in += 1
    return ( count_people_in, count_people_out )


  def on_jumping_queues( self, time : int, elevator : Elevator ):
    '''
    '''
    for queue in self.queues:
      queue_copy = queue.list()

      for x in range( len( queue ) ):
        person = queue.at( x )
        if not elevator.has_stop_on_floor( person.target_floor ):
          queue_copy[ x ] = None

    for person in queue_copy:
      if person and elevator.capacity() > 0:
        if not elevator.on_person_in( time, person, False ):
          print( "Person can't get into elevator.", file=sys.stderr )
          self.config.shutdown()
          sys.exit( 1 )
        queue.remove( person )

  def generate_beeps( self, time, elevator_states ):
    people_to_beep = []

    for x in range( self.config.num_elevators ):
      queue_copy = self.queues[ x ].list()

      for person in queue_copy:
        beep_time = time - person.time_arrival
        if beep_time > 0 and beep_time % person.beep_again == 0:
          people_to_beep.append( ( x, person ) )

    for x, person in people_to_beep:
      try:
        time, id = self.config.controller.request_beep( time, person.source_floor, person.target_floor )
        
        data_to_set = [ time ]
        data_to_set.extend( elevator_states ) 
        person.all_beeps.append( data_to_set )
        
      except Exception as e:
        print( "Received an unparsable message from controller algorithm.", e, file=sys.stderr )
        self.config.controller.stop()
        sys.exit( 1 )

      if id >= self.config.num_elevators:
        print( "Received index '{}' of elevator is out of bounds (number of elevators is {}).".format(
          id, self.config.num_elevators ), file=sys.stderr )
        self.config.controller.stop()
        sys.exit( 1 )

      if x != id:
        queue_from = self.queues[ x  ]
        queue_to   = self.queues[ id ]
        queue_from.remove( person )
        queue_to.push( person )
        
class System:
  '''
  Building representation with specific number of floors and elevators.
  
  Attributes:
    config:    Simulator configuration.    
    elevators: List of elevators of type Elevator.
    floors:    List of floors of type Floor. 
  '''
  def __init__( self, config : Config ):
    self.config    = config
    self.elevators = [ Elevator( i, config ) for i in range( config.num_elevators ) ]
    self.floors    = [ Floor( config, num ) for num in range( config.floor_min, config.floor_max+1 ) ]

  def get_floor( self, floor_number : int ) -> Floor:
    '''
    Normalize floor index.
    '''
    index = floor_number - self.config.floor_min
    return self.floors[ index ]

  def status( self ) -> list: 
    '''
    Get state of each of the elevators.
    
    Returns:
      List of pairs ( current floor of elevator, current state ).
    '''
    status = []
    for elevator in self.elevators:          
      floor_num, state = elevator.status()      
      status.append( ( floor_num, state ) )
    return status     
        
  def on_person_arrival( self, person : Person, elevator_id : int ):
    '''
    The person arrived.
    
    Args:
      person:      Person who arrived.
      elevator_id: Elevator number.
    '''
    index = person.source_floor
    floor = self.get_floor( index )
    floor.on_person_arrival( person, elevator_id )

  def on_door_opening( self, output, time : int, elevator : Elevator, floor_number : int ):
    '''
    The elevator doors at specific floor opened.
    
    Args:
      output:       Output file descriptor.
      time:         Current simulation time.
      elevator:     Elevator representation.
      floor_number: Floor number where the elevator opened doors.
    '''
    if floor_number != elevator.state.current:
      print( "Internal error occurred.", file=sys.stderr )
      self.config.shutdown()
      sys.exit( 1 ) 

    floor = self.get_floor( floor_number )
    return floor.on_door_opening( output, time, elevator )


  def on_jumping_queues( self, time : int ):
    '''
    '''
    for elevator in self.elevators:
      state = elevator.state
      floor = self.get_floor( state.current )

      if type( state ) is StateDoorOpened:
        floor.on_jumping_queues( time, elevator )

  def on_actions( self, output, time: int, actions : list ):
    '''
    Use received commands to control elevators.
    
    Args:
      output:  Output file descriptor.
      time:    Current simulation time.
      actions: List of commands to control elevators.
    '''


    if len( actions ) != self.config.num_elevators:
      print( "Invalid format of received actions.", file=sys.stderr )
      self.config.shutdown()
      sys.exit( 1 )

    for x in range( self.config.num_elevators ):
      elevator = self.elevators[ x ]
      floor_num, action = actions[ x ] 

      if action not in self.config.action_opts:
        print( "Unknown action '{}' (options are {}).".format( 
          action, self.config.action_opts ), file=sys.stderr )
        self.config.shutdown()
        sys.exit( 1 )

      if floor_num != elevator.state.current:
        print( "Unsychronized elevator '{}' states: sent floor number '{}' differs from internal number '{}'".format(
          elevator.number, floor_num, elevator.state.current ), file=sys.stderr )
        self.config.shutdown()
        sys.exit( 1 )

      state_duration = 0

      if type( elevator.state ) is StateDoorOpened:
        pin, pout = self.on_door_opening( output, time, elevator, elevator.state.current )
        elevator.state.duration = elevator.closing_steps( elevator.state.duration, pin, pout )
      state = elevator.on_action( action )

    if not self.config.respect_queue:
      self.on_jumping_queues( time )

  def generate_beeps( self, time : int ):  
    for floor in self.floors:
      states = []
      for elevator in self.elevators:
        floor_num, state = elevator.status()
        states.append( floor_num )
        states.append( state )         
      floor.generate_beeps( time, states )

class Simulator:
  '''
  Elevator simulator.
    
  Attributes:
    config: Simulator configuration. 
    time:   Current simulation time (initialized after the first person arrived).
    system: Elevator system to simulate.
  '''
  def __init__( self, config : Config ):               
    self.config = config
    self.time   = 0    
    self.system = System( config )
     
  def create_person( self, data : list ) -> Person:
    '''
    Create a person from input data.
    
    Args:
      data: Person-specific data as time of arrival, source and target floor.
    Returns:
      Instance of Person object initialized by input data. 
    '''
    p_time  = data[ 0 ]
    p_from  = data[ 1 ]
    p_to    = data[ 2 ]
    
    p_again = data[ 4 ] if data[ 4 ] != 0 else 300

    if p_again < self.config.granularity:
      p_again = self.config.granularity

    return Person( p_time, p_from, p_to, p_again )    
      
  def on_person_arrival( self, person : Person ):  
    '''
    Request elevator number on person arrival. 
    
    In case the simulation time and user time is unsynchronized, 
    the error message is printed out and the program is exited. 
    
    Args:
      person: Person who arrived.
    '''
    src = person.source_floor
    dst = person.target_floor  
    
    try:
      time, id = self.config.controller.request_beep( person.time_arrival, src, dst )
      
      states = [ person.time_arrival ]
      for elevator in self.system.elevators:
        floor_num, state = elevator.status()
        states.append( floor_num )
        states.append( state )      
      
      person.all_beeps.append( states )
      
    except Exception as e:
      print( "Received an unparsable message from controller algorithm.", e, file=sys.stderr )
      self.stop()
      sys.exit( 1 )
    
    if person.time_arrival != time:
      print( "Simulation time {} and your time {} is unsynchronized.".format( 
             person.time_arrival, time ), file=sys.stderr )  
      self.stop()
      sys.exit( 1 ) 
           
    if id >= self.config.num_elevators:
      print( "Received index '{}' of elevator is out of bounds (number of elevators is {}).".format(
             id, self.config.num_elevators ), file=sys.stderr )
      self.stop()
      sys.exit( 1 )
    
    self.config.stats.people_arrived += 1
    self.system.on_person_arrival( person, id )

  def make_step( self, out_file, time : int ):  
    '''
    Request elevator actions and make a simulation step.
    
    In case the simulation time and user time is unsynchronized, 
    the error message is printed out and the program is exited.
    
    Args:
      out_file: Initialized output file descriptor. 
      time:     Simulation time.
    '''  
    if time % self.config.granularity != 0:
    	return

    status = self.system.status()

    for i in range( self.config.num_elevators ):
      _, state = status[ i ] 
      self.config.stats.elevator_states_count[ i ][ state ] += 1        

    try:
      t, actions = self.config.controller.request_update( time, status )  
    except:
      print( "Received an unparsable message from controller algorithm", file=sys.stderr )
      self.stop()
      sys.exit( 1 )
      
    if time != t:
      print( "Simulation time {} and your time {} is unsynchronized.".format(
             time, t ), file=sys.stderr )
      self.stop()
      sys.exit( 1 )
    
    if not actions:
      print( "Invalid message received.", file=sys.stderr )
      self.stop()
      sys.exit( 1 )

    self.system.on_actions( out_file, time, actions )

  def generate_beeps( self, time : int ):
    self.system.generate_beeps( time )

  def convert_data( self, line_read : str ) -> list:
    line = line_read.split()

    if not line:
      return []

    if len( line ) != 6:
      print( "Invalid length of input data '{}'.".format(
        self.config.path_in ), file=sys.stderr )
      self.stop()
      sys.exit( 1 )

    try:
      f_time  = int( line[ 0 ] )
      f_from  = int( line[ 1 ] )
      f_to    = int( line[ 2 ] )
      f_limit = int( 100 * float( line[ 3 ] ) )
      f_again = int( line[ 4 ] )
      f_group = int( line[ 5 ] )
    except:
      print( "Invalid format of input data file '{}'".format( 
        self.config.path_in ), file=sys.stderr ) 
      self.stop()
      sys.exit( 1 )

    return [ f_time, f_from, f_to, f_limit, f_again ]

  def run( self ):
    '''
    Run elevator simulation.    
    '''
    outfile = open( self.config.path_out, 'w' )
    
    with open( self.config.path_in ) as f:
      header = f.readline()
      data   = self.convert_data( f.readline() )

      if not data:
        print( "Invalid format of input data '{}'".format(
          self.config.path_in ), file=sys.stderr )
        self.stop()
        sys.exit( 1 ) 

      self.time = data[ 0 ]      
      data_act  = data
      data_next = None      
      
      for line in f:  
        data_next = self.convert_data( line )
        # ignore empty lines 
        if not data_next:
          continue

        person = self.create_person( data_act )  
        self.on_person_arrival( person )            
        time_act  = data_act[ 0 ]
        time_next = data_next[ 0 ]                
        
        while time_act < time_next:

          if self.config.beeping_again:
            self.generate_beeps( self.time )
          
          self.make_step( outfile, self.time ) 
          self.time += 1
          time_act  += 1         
        
        data_act = data_next                    

      person = self.create_person( data_act )
      self.on_person_arrival( person )      
      
      time_limit = self.config.limit_after_eof
      time_act   = 0           
      # TODO: use attributes instead of class Statistics
      while self.config.stats.people_arrived != self.config.stats.people_served:
        if time_act > time_limit:
          config.stats.print( sys.stderr )
          not_served = self.config.stats.people_arrived - self.config.stats.people_served
          print( "FAILED: Time limit {} after last 'beep' was exceeded.".format( time_limit ), file=sys.stderr )
          print( "Increase limit in '{}' file or make controller algorithm more efficient.".format( 
                 self.config.path_config ), file=sys.stderr )
          print( "People not served: {}".format( not_served ), file=sys.stderr )
          self.stop()
          sys.exit( 1 )        
        self.make_step( outfile, self.time )        
        self.time += 1 
        time_act  += 1
      
      # make last step
      self.make_step( outfile, self.time )

      config.stats.print( sys.stderr )
      outfile.close()  

  def stop( self ):
    '''
    Stop simulation and tell controller to shut down as well.
    '''
    self.config.shutdown()
                 
if __name__=="__main__":  
  parser = argparse.ArgumentParser( description='' )
  parser.add_argument( 'controller', type=str, choices=[ 'stdio', 'socket' ],
                        help='communication channel with simulator' )
  parser.add_argument( 'filepath', type=str, help='filepath to input data' )
  parser.add_argument( '--config', type=str, help='configuration file' )
  parser.add_argument( '--output', type=str, help='output filepath' )
  args = parser.parse_args()
  
  # check if input data file exists
  if not os.path.isfile( args.filepath ):
    print( "Input data file '{}' does not exist.".format( 
           args.filepath ), file=sys.stderr )
    sys.exit( 1 )
      
  outputpath = args.output if args.output else 'output.txt'
  configpath = args.config if args.config else 'config.ini'

  config = Config( args.controller, configpath, args.filepath, outputpath )    

  simulator = Simulator( config )
  simulator.run()
  simulator.stop()
  
